from django.contrib.auth.models import User
from rest_framework import serializers

from api.models import Operation


class UserOperationSerializer(serializers.ModelSerializer):

    class Meta:
        model = Operation
        fields = '__all__'

    def to_internal_value(self, data):
        data['user'] = self.context['request'].user.id
        recipient_email = data['recipient']
        return super().to_internal_value(data)