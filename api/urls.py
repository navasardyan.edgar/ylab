from django.urls import include, path

from rest_framework.routers import SimpleRouter
from api.views import OperationsListView


router = SimpleRouter()
router.register(r'operations', OperationsListView, basename='operations')

urlpatterns = [
    path('', include((router.urls))),
]