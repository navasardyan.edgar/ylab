from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet
from api.models import Operation
from api.serializers import (UserOperationSerializer)


class OperationsListView(
            mixins.CreateModelMixin,
            mixins.ListModelMixin,
            GenericViewSet):
    serializer_class  = UserOperationSerializer

    def get_queryset(self):
        return Operation.objects.filter(user=self.request.user)