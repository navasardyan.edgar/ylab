from django.db import models
from django.contrib.auth.models import User


class Operation(models.Model):
    user = models.ForeignKey(User, on_delete = 'PROTECT')
    recipient = models.ForeignKey(User, on_delete = 'PROTECT', related_name='operations')
    amount = models.DecimalField(max_digits=6, decimal_places=2)

    class Meta:
        db_table = 'operation'

    def __str__(self):
        return 'User {} sent {}{} to {}'.format(
            self.user,
            self.amount,
            self.user.balance_currency,
            self.recipient)
