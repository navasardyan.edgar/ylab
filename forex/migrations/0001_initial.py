# Generated by Django 2.2.13 on 2020-06-10 08:30

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Rates',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('base', models.CharField(max_length=3)),
                ('target', models.CharField(max_length=3)),
                ('value', models.DecimalField(decimal_places=4, max_digits=14)),
                ('is_active', models.BooleanField()),
                ('as_of_dt', models.DateTimeField()),
            ],
            options={
                'db_table': 'rates',
            },
        ),
    ]
