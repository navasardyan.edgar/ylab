import logging
import requests
from requests.exceptions import HTTPError

logger = logging.getLogger(__name__)

def download(base_currency=None, target_currencies=None):
    target_currencies = ['USD', 'GBP']
    url = 'https://api.exchangeratesapi.io/latest'
    params = {'symbols': ','.join(target_currencies)}

    try:
        response = requests.get(url, params=params)
        response.raise_for_status()
    except HTTPError as err:
        logger.error('HTTP error occurred while downloading fx rates: {}'.format(err))

    except Exception as err:
        logger.error('Unknown error occurred: {err}'.format(err))  # Python 3.6

    return response
