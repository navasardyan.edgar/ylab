import requests
from django.core.management.base import BaseCommand
from forex.data_import import download
from forex.models import HistoricalRate


class Command(BaseCommand):

    def handle(self, *args, **kwargs):
        response = download()
        json_response = response.json()
        base_currency = json_response['base']
        instances = []
        for target_currency, rate in json_response['rates'].items():
            instance = HistoricalRate(
                base=base_currency,
                target=target_currency,
                value=rate,
                is_active=True)
            instances.append(instance)
        HistoricalRate.objects.update(is_active=False)
        HistoricalRate.objects.bulk_create(instances)


