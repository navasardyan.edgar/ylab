from django.db import models


class HistoricalRate(models.Model):
    base = models.CharField(max_length=3)
    target = models.CharField(max_length=3)
    value = models.DecimalField(max_digits=14, decimal_places=4)
    is_active = models.BooleanField()

    class Meta:
        db_table = 'rates'
