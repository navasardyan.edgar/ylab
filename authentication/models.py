from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import gettext as _


CURRENCY_CHOICES = (
    ('eur', _('EUR')),
    ('usd', _('USD')),
    ('gbp', _('GBP')),
    ('rub', _('RUB')),
    ('btc', _('BTC')),
)


class UserProfile(models.Model):
    user = models.OneToOneField(User, on_delete='CASCADE')
    initial_balance = models.DecimalField(max_digits=6, decimal_places=2)
    account_currency = models.CharField(max_length=9, choices=CURRENCY_CHOICES, default='rub')

    class Meta:
        db_table = 'user_profile'

    def __str__(self):
        return self.user.email