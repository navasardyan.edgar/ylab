from django.urls import include, path

from rest_framework.routers import SimpleRouter
from rest_framework_jwt.views import ObtainJSONWebToken

from authentication.views import UserViewSet
from authentication.serializers import CustomJWTSerializer


router = SimpleRouter()
router.register(r'users', UserViewSet, basename='users')

urlpatterns = [
    path('', include((router.urls))),
    path('login/', ObtainJSONWebToken.as_view(serializer_class=CustomJWTSerializer)),
]

