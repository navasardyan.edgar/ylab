from rest_framework import mixins
from rest_framework.viewsets import GenericViewSet
from authentication.models import UserProfile
from authentication.serializers import (UserProfileSerializer)


class UserViewSet(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  GenericViewSet):
    queryset = UserProfile.objects.filter(user__is_active=True)
    serializer_class  = UserProfileSerializer
    permission_classes = ()